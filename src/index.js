import React from 'react';
import ReactDOM from 'react-dom';
import ToDo from './components/ToDo'
import './styles/global.scss';


ReactDOM.render(
  <React.StrictMode>
    <ToDo />
  </React.StrictMode>,
  document.getElementById('root')
);
