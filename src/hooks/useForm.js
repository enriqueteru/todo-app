import { useState } from 'react';

export const useForm = (INITIAL_STATE = {}) => {
  const [formValues, setFormValues] = useState(INITIAL_STATE);


  const reset = () => { 
      setFormValues(INITIAL_STATE)
  }


  const handleInputChange = ({ target }) => {
    setFormValues({
      ...formValues,
      [target.name]: target.value,
    });
  };

  return [formValues, handleInputChange, reset];
};
