import React, { useEffect, useReducer } from 'react';
import { todoReducer } from '../reducers/todoReducer';
import ToDoList from './ToDoList';
import ToDoAdd from './ToDoAdd';
import Header from '../shared/Header';

const init = () => { 
  return JSON.parse(localStorage.getItem('todos') ) || []
}

function ToDo() {

  const [todos, dispatch] = useReducer(todoReducer, [], init);

  useEffect(() => {
    localStorage.setItem('todos', JSON.stringify(todos) )
  }, [todos]);
  

  const handleDelete = (todoID) =>  {
  
    const action = {
      type : 'DELETE_TODO',
      payload: todoID
    }

    dispatch(action)
  }

  const handleToggle = (todoID) =>  {
    const action = {
      type : 'TOGGLE_TODO',
      payload: todoID
    }
   dispatch( action )

  }


  const handleAddTodo = (newTodo) => { 

    const action = { 
      type: 'ADD_TODO',
      payload: newTodo
    }
     dispatch(action);
    
  }


  
  return (
<>

<Header />

     <div className='row'>
      <section className='col1'>
      <ToDoAdd  handleAddTodo={handleAddTodo} />
      </section>

      <section className='col2'>
        <ToDoList todos={todos} handleToggle={handleToggle} handleDelete={handleDelete}/>
        {
          todos.length <= 0 &&
          <h4 style={{textAlign:'center'}}> Nothing to do. That´s awesome!</h4>
        }
      </section>

    </div>
    </>

  );
}

export default ToDo;
