import React from 'react';
import ToDoListItem from './ToDoListItem';

const ToDoList = ({ todos, handleDelete, handleToggle }) => {
  return (
    <>
     <div >
      <h1>To Do ({todos.length})</h1>
    </div>
      {todos.map((todo, i) => (
        <div
          key={todo.id}
          className='none'
          onClick={() => handleToggle(todo.id)}
        >
          <ToDoListItem todo={todo} i={i} handleDelete={handleDelete} />
        </div>
      ))}
    </>
  );
};

export default ToDoList;
