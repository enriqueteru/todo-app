import React from 'react';
import { useForm } from '../hooks/useForm';

const ToDoAdd = ({ handleAddTodo }) => {
  const [{ todo }, handleInputChange, reset] = useForm({
    todo: '',
  });

  const handleSubmit = (ev) => {
    ev.preventDefault();

    if (todo.trim().length <= 1) {
      return alert('Add a new To Do task');
    }

    const newTodo = {
      id: new Date().getTime(),
      todo: todo,
      done: false,
    };

    handleAddTodo(newTodo);
    reset();
  };

  return (
    <>
      <form 
      className='form' 
      onSubmit={handleSubmit}>
        <input
          className='form__input'
          type='text'
          name='todo'
          onChange={handleInputChange}
          value={todo}
          placeholder='Something to do?'
          autoComplete='off'
        />
        <button className='btn' type='submit'> New To Do</button>
      </form>
    </>
  );
};

export default ToDoAdd;
