import React from 'react';

const ToDoListItem = ({todo, i, handleDelete}) => {
  return <div className='item'>
   <p className={ 'item__list ' + (!todo.done ? 'none' : 'done')} >{i + 1} — {todo.todo}</p> 
          <button className='item_btn'
          onClick={()=> handleDelete(todo.id)}
          > 
            X
          </button>
  
  </div>;
};

export default ToDoListItem;
